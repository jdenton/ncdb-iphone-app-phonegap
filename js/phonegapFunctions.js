var storageObj = {
    
}

var cameraObj = {
    capturePhoto : function() {
        // Take picture using device camera and retrieve image as base64-encoded string
        navigator.camera.getPicture(cameraObj.onPhotoURISuccess, cameraObj.onFail, { 
            quality: 30,
            destinationType: destinationType.FILE_URI
        });
    },
    getPhoto : function(source) {
        if (source == null) {
            source = pictureSource.PHOTOLIBRARY;
        }
        // Retrieve image file location from specified source
        navigator.camera.getPicture(cameraObj.onPhotoURISuccess, cameraObj.onFail, {
            quality: 30,
            destinationType: destinationType.FILE_URI,
            sourceType: source
        });
    },
    onPhotoURISuccess : function(imageURI) {
        pictureStore.add({'ThumbnailURL': imageURI});
        inventoryUpdateObj.updateAttachCount();

        var largeImage = $('#smallImage');
        largeImage.attr('src', imageURI);
        largeImage.show();

        var options = new FileUploadOptions();
        options.fileKey = "file";
        options.mimeType = "image/jpeg";

        var params = new Object();
        params.fromMobile = '1';
        params.authKey = authKey;
        options.params = params;

        var ft = new FileTransfer();
        ft.upload(imageURI, siteURL + 'filemanager/uploadFileHandler', cameraObj.onSuccess, cameraObj.onFail, options);


        //var filePostURL = siteURL + 'filemanager/uploadFileHandler';
        //$.post(filePostURL, {imageData:imageData}, function(data){});
	},
    onFail : function(message) {
		alert('Oops: ' + message);
	},
    onSuccess : function(r) {
        alert("Code = " + r.responseCode);
        alert("Response = " + r.response);
    }
}