var modelsObj = {
    initModels : function() {
        Ext.regModel('ModelSTList', {
            fields: [
                'CatID', 'CatTitle'
            ]
        });

        Ext.regModel('ModelRoomSelect', {
            fields: ['RoomID', 'Title']
        });

        Ext.regModel('PropertyList', {
            fields: [
                {name: 'AvatarURL',  type: 'string', defaultValue: siteURL+'images/avatars/stockAvatar46.png'},
                'PlaceID',
                'PlaceName',
                'Address',
                'City',
                'State',
                'Zip'
            ]
        });

        Ext.regModel('RoomSelect', {
            fields: ['RoomID', 'RoomName']
        });

        Ext.regModel('RoomList', {
            fields: ['RoomID', 'RoomName', 'InventoryItems']
        });

        Ext.regModel('PictureList', {
            fields: ['ThumbnailURL']
        });

        Ext.regModel('InventoryRoomModel', {
            fields: [
                {name: 'RoomID', type: 'string'},
                {name: 'RoomName', type: 'string'},
                {name: 'Inventory', type: 'array'},
                {name: 'RoomTags',  type: 'string'}
            ]
        });

        Ext.regModel('InventoryList', {
            fields: [
                {name: 'AvatarURL',  type: 'string', defaultValue: siteURL+'images/avatars/stockAvatar46.png'},
                {name: 'InventoryID', type: 'string'},
                {name: 'ItemName', type: 'string'},
                {name: 'ItemMake', type: 'string'},
                {name: 'ItemModel', type: 'string'},
                {name: 'Description',  type: 'string'},
                {name: 'PriceReplace',  type: 'string'}
            ]
        });
    }
}