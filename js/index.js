document.addEventListener('deviceready', onDeviceReady, false);

function onDeviceReady() {
    pictureSource = navigator.camera.PictureSourceType;
    destinationType = navigator.camera.DestinationType;
}

Ext.ns('ncdb');
Ext.setup({
    icon: siteURL+'images/mobile/appIcon.png',
    tabletStartupScreen: siteURL+'images/mobile/tablet_startup.png',
    phoneStartupScreen: siteURL+'images/mobile/phone_startup.png',
    glossOnIcon: true,
    onReady: function() {
        loadMask  = new Ext.LoadMask(Ext.getBody(), {msg: 'Loading stuff...'});
        loadMask.show();
        mainApp.initApp();
    }    
});