var mainApp = {
    initApp : function() {
        modelsObj.initModels();
        mainApp.getCommonAppStores();

        var stPanel        = selectedTopicsObj.init();
        var stContentPanel = selectedTopicObj.init();

        var mainPanel = new Ext.Panel({
            id: 'mainPanel',
            fullscreen: true,
            ui: 'dark',
            layout: 'card',
            cardSwitchAnimation: {
                type: 'slide',
                cover: true
            },
            activeItem: 0,
            items: [
                stPanel,
                stContentPanel
            ]
        });
    },
    getCommonAppStores : function() {
        /*
        pictureStore = new Ext.data.Store({
            model: 'PictureList',
            storeId: 'pictureStore',
            proxy: {
                type: 'memory',
                reader: {
                    type: 'json',
                    root: ''
                }
            }
        });
        */
    }
}