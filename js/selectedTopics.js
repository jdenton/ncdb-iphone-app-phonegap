var selectedTopicsObj = {
    init : function() {
        var stStore = new Ext.data.Store({
            model: 'ModelSTList',
            sorters: 'CatTitle',
            
            getGroupString : function(record) {
                return record.get('CatTitle')[0];
            },
            proxy: {
                type: 'ajax',
                url: siteURL+'mobileFunctions.php',
                reader: {
                    type: 'json',
                    root: ''
                }
            }
        });

        var stListControl = new Ext.List({
            itemTpl: $('#stList').html(),
            grouped: true,
            singleSelect: true,
            loadingText: null,
            store: stStore,
            onItemDisclosure: function(record, btn, index) {
                Ext.getCmp('mainPanel').layout.setActiveItem(1,'slide');
                var listItem = record.store.getAt(index);
                var topicCatID = listItem.data.CatID;
                var catTitle = listItem.data.CatTitle;

                if (catTitle.length>15) {
                    catTitle = catTitle.substr(0,13)+'...';
                }

                Ext.getCmp('stContentPanel').dockedItems.items[0].setTitle(catTitle);
                /*
                 * Get the selected topic content
                 */
                loadMask.show();
                Ext.Ajax.request({
                    method: 'POST',
                    params: { getSelectedTopicContent: '1', topicCatID: topicCatID },
                    url: siteURL+'mobileFunctions.php',
                    success: function(response, opts) {
                        Ext.getCmp('stContentPanel').update(response.responseText);
                        loadMask.hide();
                    }
                });
            }
        });

        var stPanel = new Ext.Panel({
            layout: 'fit',
            fullscreen: true,
            id: 'stListPanel',
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                defaults: {
                    iconMask: true
                },
                items: [{                    
                    xtype : 'searchfield',
                    placeHolder: 'Search',
                    listeners: {
                        scope: this,
                        keyup: function(field) {
                            var value = field.getValue();
                            if (!value) {
                                stStore.filterBy(function() {
                                    return true;
                                });
                            } else {
                                var searches = value.split(' '),
                                    regexps  = [],
                                    i;

                                for (i = 0; i < searches.length; i++) {
                                    if (!searches[i]) return;
                                    regexps.push(new RegExp(searches[i], 'i'));
                                }

                                stStore.filterBy(function(record) {
                                    var matched = [];

                                    for (i = 0; i < regexps.length; i++) {
                                        var search = regexps[i];
                                        if (record.get('CatTitle').match(search) || record.get('CatTitle').match(search)) matched.push(true);
                                        else matched.push(false);
                                    }

                                    if (regexps.length > 1 && matched.indexOf(false) != -1) {
                                        return false;
                                    } else {
                                        return matched[0];
                                    }
                                });
                                stListControl.scroller.scrollTo({x: 0,y: 0});
                            }
                        }
                    }
                },{
                    xtype: 'spacer'
                },{
                    ui: 'confirm',
                    iconCls: 'info',
                    handler: function() {
                        if (!this.infoPopup) {
                            this.infoPopup = selectedTopicsSupportObj.makeInfoPopup();
                        }
                        this.infoPopup.show('pop');
                    }
                }]
            }],
            items: [
                    stListControl
                ],
            listeners: {
                render: function() {
                    /*
                     * If our store is not loaded, then load and bind.
                     */
                    if (!Ext.isDefined(stStore.totalLength) && !stStore.lastOptions) {
                        stStore.proxy.actionMethods.read = 'POST';
                        stStore.proxy.extraParams = {getSelectedTopics: '1'}
                        stStore.load();
                        stListControl.bindStore(stStore);
                    }
                    loadMask.hide();
                }
            }
        });        

        return stPanel;
    }
}

var selectedTopicObj = {
    init : function() {
        var stContentPanel = new Ext.Panel({
            layout: 'fit',
            fullscreen: true,
            scroll: 'vertical',
            width: 320,
            height: 450,
            id: 'stContentPanel',
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                defaults: {
                    iconMask: true
                },
                items: [{
                    iconCls: 'home',
                    ui: 'back',
                    handler: function() {
                        Ext.getCmp('mainPanel').layout.setActiveItem(0,{ type: 'slide', reverse: true });
                    }
                },{
                    xtype: 'spacer'
                },{
                    ui: 'confirm',
                    iconCls: 'info',
                    handler: function() {
                        if (!this.infoPopup) {
                            this.infoPopup = selectedTopicsSupportObj.makeInfoPopup();
                        }
                        this.infoPopup.show('pop');
                    }
                }]
            }]
        });

        return stContentPanel;
    }
}

var selectedTopicsSupportObj = {
    makeInfoPopup : function() {
        var infoPopup = new Ext.Panel({
            floating: true,
            modal: true,
            centered: true,
            width: 300,
            height: 200,
            styleHtmlContent: true,
            scroll: 'vertical',
            html: $('#infoContainer').html(),
            dockedItems: [{
                dock: 'top',
                xtype: 'toolbar',
                title: 'About NCDB Mobile'
            }]
        });

        return infoPopup;
    }
}